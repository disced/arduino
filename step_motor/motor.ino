#include <Stepper.h>

#define STEPS     64
#define SPEED     350
#define TOT_STEPS 1024

#define PIN_S_ONE    8
#define PIN_S_TWO    9
#define PIN_S_THREE  10
#define PIN_S_FOUR   11

enum      TMov         {UP, DOWN};
const int movement[] = {TOT_STEPS, -TOT_STEPS};
Stepper   stepper(STEPS, PIN_S_ONE, PIN_S_TWO, PIN_S_THREE, PIN_S_FOUR);

void setup() {
  stepper.setSpeed( SPEED );
}

void loop() {
  stepper.step( movement[UP] );
  delay(10);
  //stepper.step( movement[DOWN] );
  //delay(10);
}
